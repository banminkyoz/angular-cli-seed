import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sample-component',
  templateUrl: './sample.component.html',
  styleUrls: ['./sample.component.scss']
})
export class SampleComponent implements OnInit {

  constructor(private _router: Router) { }

  ngOnInit() {
  }

  goNext() {
    this._router.navigate(['/seed/sample2']);
  }

}
