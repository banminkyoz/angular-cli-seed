import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TestComponent } from './test.component';

import { SampleComponent } from '../../lib/components/sample/sample.component';
import { Sample2Component } from '../../lib/components/sample-2/sample-2.component';

const adminRoutes: Routes = [
  {
    path: '',
    component: TestComponent,
    children: [
      {
        path: '',
        children: [
          { path: 'sample', component: SampleComponent }
        ]
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(adminRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class TestRoutingModule { }
